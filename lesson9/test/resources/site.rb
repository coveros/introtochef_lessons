property :homepage, String, default: '<h1>Hello world!</h1>'
action :create do
  if node[:platform_family].include?("rhel")
package 'httpd'
    service 'httpd' do
        action [:enable, :start]
    end
    file '/var/www/html/index.html' do
        content homepage
    end
  end
  if node[:platform_family].include?("debian")
    package 'apache2'
    service 'apache2' do
        action [:enable, :start]
    end
    file '/var/www/html/index.html' do
        content homepage
    end
  end
end
action :delete do
  if node[:platform_family].include?("rhel")
    package 'httpd' do
      action :delete
    end
  end
  if node[:platform_family].include?("debian")
    package 'httpd' do
      action :delete
    end
  end
end



