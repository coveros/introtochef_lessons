# test::site


package 'httpd'

template '/var/www/html/index.html' do
    source 'index.html.erb'
    owner  'root'
    group  'apache'
    mode   '0640'
    notifies :restart, 'service[httpd]'
    variables ({
        :greetname => "Gene"
    })
end



service 'httpd' do
	action [ :enable, :start ]
end
