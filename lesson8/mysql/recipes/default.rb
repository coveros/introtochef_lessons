#
# Cookbook:: mysql
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'mariadb'
package 'mariadb-server'

service 'mariadb' do
  supports :status => true, :restart => true, :reload => true
  action [:start, :enable]
end



bash "Set App User" do
	code <<-EOH
		echo "create database speaker_db;" |  mysql -u root
		echo "create user 'tomcat8' IDENTIFIED BY 'tomcat8';" |  mysql -u root
		echo "create user 'tomcat8'@'localhost' IDENTIFIED BY 'tomcat8';" |  mysql -u root
		echo "GRANT ALL PRIVILEGES ON *.* TO 'tomcat8'@'%' IDENTIFIED BY 'tomcat8';" |  mysql -u root
		echo "GRANT ALL PRIVILEGES ON *.* TO 'tomcat8'@'localhost' IDENTIFIED BY 'tomcat8';" |  mysql -u root
		echo "FLUSH PRIVILEGES;" |  mysql -u root
	EOH
end