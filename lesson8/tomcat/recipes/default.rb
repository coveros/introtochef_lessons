#
# Cookbook:: tomcat
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.


package 'java-1.8.0-openjdk'

cookbook_file '/opt/tomcat.tar.gz' do
  source 'tomcat.tar.gz'
  owner 'root'
  group 'root'
  mode '0644'
end

group 'tomcat' do
  action :create
end

user 'tomcat' do
  action :create
  comment 'tomcat'
  group 'tomcat'
  home '/dev/null'
  shell '/bin/nologin'
end

bash 'install tomcat' do
  user 'root'
  cwd '/opt'
  code <<-EOH
  	tar xzvf tomcat.tar.gz;
  	ln -s /opt/apache-tomcat-8.5.4 /opt/tomcat
  	chown -R tomcat:tomcat *tomcat*
  EOH
end


template '/etc/systemd/system/tomcat.service' do
  source 'tomcat.service.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

cookbook_file '/opt/tomcat/webapps/hangman.war' do
  source 'hangman.war'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
end

template '/opt/tomcat/lib/env.properties' do
  source 'env.properties.erb'
  owner 'tomcat'
  group 'tomcat'
  mode '0644'
end



service 'tomcat' do
  supports :status => true, :restart => true, :reload => true
  action [:start, :enable]
end
